package com.nertlab.cas.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.google.gson.Gson;
import com.nertlab.cas.ApplicationContstant;
import com.nertlab.cas.R;
import com.nertlab.cas.other.PayementAdapter;
import com.nertlab.cas.other.Payment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements DatePickerDialog.OnDateSetListener {

    private RecyclerView recyclerView;
    private PayementAdapter payementAdapter;
    private List<Payment> paymentList;
    private Calendar myCalendar;
    private TextView selected_week, totalPay, totalAttendance, totalStudent, last7days;
    private ImageView btn_pop;
    private String[] colors = {"#A082FF", "#FF7588", "#FF9E68", "#00E67E"};
    private String[] darkColors = {"#834EF5", "#FF4561", "#FF7A2E", "#00D750"};

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    private SweetAlertDialog pDialog;
    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        //Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/abc.ttf");

        sharedpreferences = getActivity().getSharedPreferences("my-pref", Context.MODE_PRIVATE);


        View rootView = inflater.inflate(R.layout.fragment_home,
                container, false);

        totalPay = (TextView) rootView.findViewById(R.id.totalPay);
        TextView lblPay = (TextView) rootView.findViewById(R.id.lblPay);
        TextView lblStuden = (TextView) rootView.findViewById(R.id.lblStuden);
        totalStudent = (TextView) rootView.findViewById(R.id.totalStudent);
        TextView lblAttendence = (TextView) rootView.findViewById(R.id.lblAttendence);
        last7days = (TextView) rootView.findViewById(R.id.last7days);
        totalAttendance = (TextView) rootView.findViewById(R.id.totalAttendance);
        /*totalPay.setTypeface(font);
        //lblPay.setTypeface(font);
        //lblStuden.setTypeface(font);
        totalStudent.setTypeface(font);
        //lblAttendence.setTypeface(font);
        totalAttendance.setTypeface(font);*/


        selected_week = (TextView) rootView.findViewById(R.id.selected_week);

        btn_pop = (ImageView) rootView.findViewById(R.id.btn_pop);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        paymentList = new ArrayList<>();
        payementAdapter = new PayementAdapter(getActivity(), paymentList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(payementAdapter);


        myCalendar = Calendar.getInstance();

        btn_pop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        HomeFragment.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });

        updateLabel();
        return rootView;
    }



    private void updateLabel() {

        String myFormat = "yyyy-M-d"; //In which you need put here
        String myFormat1 = "d MMM"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);

        Date end = myCalendar.getTime();

        myCalendar.add(Calendar.DATE, -6);
        Date start = myCalendar.getTime();

        last7days.setText("Last 7 days : ");
        selected_week.setText(sdf1.format(start) + " to " + sdf1.format(end));
        editor = sharedpreferences.edit();
        editor.putString("start_date", sdf.format(start).toString());
        editor.putString("end_date", sdf.format(end).toString());
        editor.putString("start_date_view", sdf1.format(start).toString());
        editor.putString("end_date_view", sdf1.format(end).toString());
        editor.commit();
        paymentList.clear();
        prepareList(sdf.format(start).toString(), sdf.format(end).toString());
    }

    private void prepareList(String dateStart, String dateEnd) {
        pDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        //pDialog.show();
        String class_id = sharedpreferences.getString("class_id", "");
        String token = sharedpreferences.getString("token", "");
        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cfms_payments_from", dateStart);
            jsonObject.put("cfms_payments_to", dateEnd);
            jsonObject.put("class_id", class_id);
            jsonObject.put("cfms_token", token);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        okhttp3.RequestBody body = RequestBody.create(JSON, jsonObject.toString());


        okhttp3.Request request = new Request.Builder()
                .url(ApplicationContstant.overview)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.cancel();
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseString = response.body().string();
                    final JSONObject object = new JSONObject(responseString);
                    final String totalPayments = object.get("total_payments").toString();
                    final String totalAttendances = object.get("total_attendance").toString();
                    final String totalStudents = object.get("total_students").toString();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            totalPay.setText(totalPayments);
                            totalStudent.setText(totalStudents);
                            totalAttendance.setText(totalAttendances);
                        }
                    });
                    JSONArray jsonArray = object.getJSONArray("overview_payments");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Gson gson = new Gson();
                        final Payment payment = gson.fromJson(jsonObject.toString(), Payment.class);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateUI(payment);
                            }
                        });

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private int looper = 0;

    private void updateUI(Payment payment) {
        if (looper == colors.length) {
            looper = 0;
        }
        if (payment != null) {
            payment.setColor(colors[looper]);
            payment.setDarkColor(darkColors[looper]);
            paymentList.add(payment);
            payementAdapter.notifyDataSetChanged();
            looper++;
        }
        pDialog.cancel();

        if(paymentList.isEmpty()){
            new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Oops.. ")
                    .setContentText("No, Records Available")
                    .setConfirmText("Ok")
                    .show();
        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {

            String month[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

            String dateStart = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
            String dateEnd = yearEnd + "-" + (monthOfYearEnd + 1) + "-" + dayOfMonthEnd;

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
            try {
                Date dateEObj = sdf.parse(dateEnd);
                Date dateSObj = sdf.parse(dateStart);

                Calendar now = Calendar.getInstance();
                Calendar dateStartObj = Calendar.getInstance();
                Calendar dateEndObj = Calendar.getInstance();
                dateEndObj.setTime(dateEObj);
                dateStartObj.setTime(dateSObj);
                if (dateEndObj.before(now) && dateStartObj.before(dateEndObj)) {
                    last7days.setVisibility(View.GONE);
                    selected_week.setText(dayOfMonth + " " + month[monthOfYear] + "  to  " + dayOfMonthEnd + " " + month[monthOfYearEnd]);
                    editor = sharedpreferences.edit();
                    editor.putString("start_date", dateStart);
                    editor.putString("end_date", dateEnd);
                    editor.putString("start_date_view", dayOfMonth + " " + month[monthOfYear]);
                    editor.putString("end_date_view", dayOfMonthEnd + " " + month[monthOfYearEnd]);
                    editor.commit();
                    paymentList.clear();
                    prepareList(dateStart, dateEnd);
                } else {
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Invalid Date")
                            .setContentText("Please choose a valid date range")
                            .show();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }


    }
}


