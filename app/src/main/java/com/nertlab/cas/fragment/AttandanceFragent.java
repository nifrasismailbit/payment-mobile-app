package com.nertlab.cas.fragment;


import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.other.Attendance;
import com.nertlab.cas.other.AttendanceAdapter;
import com.nertlab.cas.other.PayementAdapter;
import com.nertlab.cas.other.Payment;
import com.nertlab.cas.other.Student;
import com.nertlab.cas.other.StudentAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class AttandanceFragent extends Fragment implements com.borax12.materialdaterangepicker.date.DatePickerDialog.OnDateSetListener {


    private RecyclerView recyclerView;
    private AttendanceAdapter attendanceAdapter;
    private List<Attendance> attendanceList;
    private ImageView btn_pop;
    private Calendar myCalendar;

        private SharedPreferences sharedpreferences;
        private SharedPreferences.Editor editor;

    private TextView lblDate,lblMonth;
    public AttandanceFragent() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_attandance,
                container, false);

        sharedpreferences = getActivity().getSharedPreferences("my-pref", Context.MODE_PRIVATE);


        btn_pop = (ImageView) rootView.findViewById(R.id.btn_pop);

        lblDate = (TextView) rootView.findViewById(R.id.lblDate);
        lblMonth = (TextView) rootView.findViewById(R.id.lblMonth);
        myCalendar = Calendar.getInstance();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        attendanceList = new ArrayList<>();
        attendanceAdapter = new AttendanceAdapter(getActivity(), attendanceList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(attendanceAdapter);


        prepareList();

        btn_pop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                com.borax12.materialdaterangepicker.date.DatePickerDialog dpd = com.borax12.materialdaterangepicker.date.DatePickerDialog.newInstance(
                        AttandanceFragent.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
            }
        });
        updateLabel();
        return rootView;
    }

    private void updateLabel() {

        String myFormat = "MMM"; //In which you need put here
        String myFormat1 = "dd"; //In which you need put here

        String myFormat2 = "yyyy-M-d"; //In which you need put here
        String myFormat3 = "d MMM"; //In which you need put here

        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);
        SimpleDateFormat sdf2 = new SimpleDateFormat(myFormat2, Locale.US);
        SimpleDateFormat sdf3 = new SimpleDateFormat(myFormat3, Locale.US);

        Date end = myCalendar.getTime();

        myCalendar.add(Calendar.DATE, -6);
        Date start = myCalendar.getTime();

        lblDate.setText(sdf1.format(start) + " - " + sdf1.format(end));
        lblMonth.setText(sdf.format(start) + " to " + sdf.format(end));
        editor = sharedpreferences.edit();
        editor.putString("attendance_start_date", sdf2.format(start).toString());
        editor.putString("attendance_end_date", sdf2.format(end).toString());
        editor.putString("attendance_start_date_view", sdf3.format(start).toString());
        editor.putString("attendance_end_date_view", sdf3.format(end).toString());
        editor.commit();
        attendanceList.clear();
        //TODO need to pass the date paramenter to filter
        prepareList();
    }

    private void prepareList() {
        Attendance attendance1 = new Attendance("2018 A/L SAT 6.15.8.15","ENGLISH","196");
        attendanceList.add(attendance1);
        Attendance attendance2 = new Attendance("2019A/L FRI 11.15-2.15","ENGLISH","284");
        attendanceList.add(attendance2);
        Attendance attendance3 = new Attendance("2018 A/L SAT 6.15.8.15","ENGLISH","4");
        attendanceList.add(attendance3);
        Attendance attendance4 = new Attendance("2018 A/L SAT 6.15.8.15","ENGLISH","4");
        attendanceList.add(attendance4);

        attendanceAdapter.notifyDataSetChanged();
    }


    @Override
    public void onDateSet(com.borax12.materialdaterangepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
        String month[] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

        String dateStart = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
        String dateEnd = yearEnd + "-" + (monthOfYearEnd + 1) + "-" + dayOfMonthEnd;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
        try {
            Date dateEObj = sdf.parse(dateEnd);
            Date dateSObj = sdf.parse(dateStart);

            Calendar now = Calendar.getInstance();
            Calendar dateStartObj = Calendar.getInstance();
            Calendar dateEndObj = Calendar.getInstance();
            dateEndObj.setTime(dateEObj);
            dateStartObj.setTime(dateSObj);
            if (dateEndObj.before(now) && dateStartObj.before(dateEndObj)) {
                lblDate.setText(dayOfMonth + " - " + dayOfMonthEnd );
                lblMonth.setText(month[monthOfYear] + " to " + month[monthOfYearEnd] );
                editor = sharedpreferences.edit();
                editor.putString("attendance_start_date", dateStart);
                editor.putString("attendance_end_date", dateEnd);
                editor.putString("attendance_start_date_view", dayOfMonth + " " + month[monthOfYear]);
                editor.putString("attendance_end_date_view", dayOfMonthEnd + " " + month[monthOfYearEnd]);
                editor.commit();
            } else {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Invalid Date")
                        .setContentText("Please choose a valid date range")
                        .show();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}

