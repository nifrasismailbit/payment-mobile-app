package com.nertlab.cas.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nertlab.cas.R;
import com.nertlab.cas.beans.MyClassBean;
import com.nertlab.cas.other.Attendance;
import com.nertlab.cas.other.MyClassAdapter;
import com.nertlab.cas.other.PayementAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyClassFragment extends Fragment {


    private RecyclerView recyclerView;
    private List<MyClassBean> myClassBeanList;
    private MyClassAdapter myClassBeanAdapter;

    public MyClassFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_my_class, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        myClassBeanList = new ArrayList<>();
        myClassBeanAdapter = new MyClassAdapter(getActivity(), myClassBeanList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(myClassBeanAdapter);

        prepareList();
        return rootView;
    }

    private void prepareList() {
        MyClassBean myClassBean1 = new MyClassBean("2018 A/L SAT 6.15.8.15","ENGLISH","FRIDAY","1500","196");
        myClassBeanList.add(myClassBean1);
        MyClassBean myClassBean2 = new MyClassBean("2018 A/L SAT 6.15.8.15","ENGLISH","FRIDAY","1500","196");
        myClassBeanList.add(myClassBean1);
        MyClassBean myClassBean3 = new MyClassBean("2018 A/L SAT 6.15.8.15","ENGLISH","FRIDAY","1500","196");
        myClassBeanList.add(myClassBean1);
        MyClassBean myClassBean4 = new MyClassBean("2018 A/L SAT 6.15.8.15","ENGLISH","FRIDAY","1500","196");
        myClassBeanList.add(myClassBean1);

        myClassBeanAdapter.notifyDataSetChanged();
    }


}
