package com.nertlab.cas.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.nertlab.cas.R;
import com.nertlab.cas.beans.AbsenceBean;
import com.nertlab.cas.other.AbsenceAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.nertlab.cas.R.id.month;

/**
 * A simple {@link Fragment} subclass.
 */
public class AbsenseFragment extends Fragment implements OnDateSetListener {


    private RecyclerView recyclerView;
    private List<AbsenceBean> absenceBeanList;
    private AbsenceAdapter absenceAdapter;
    private Calendar calendar;
    private TextView date,month;
    private SweetAlertDialog pDialog;

    public AbsenseFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_absense, container, false);

        calendar = Calendar.getInstance();

        final DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false);

        date = rootView.findViewById(R.id.date);
        month = rootView.findViewById(R.id.month);
        rootView.findViewById(R.id.btn_cal).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datePickerDialog.show(getActivity().getSupportFragmentManager(), "DATE_PICKER");
            }
        });

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        absenceBeanList = new ArrayList<>();
        absenceAdapter = new AbsenceAdapter(getActivity(), absenceBeanList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(absenceAdapter);

        prepareList();
        updateLabel();
        return rootView;
    }

    private void updateLabel() {

        String myFormat = "dd"; //In which you need put here
        String myFormat1 = "MMMM"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        SimpleDateFormat sdf1 = new SimpleDateFormat(myFormat1, Locale.US);

        Date now = calendar.getTime();

        date.setText(sdf.format(now)+"");
        month.setText(sdf1.format(now));
    }

    private void prepareList() {
        AbsenceBean absenceBean1 = new AbsenceBean("2018 A/L SAT 6.15.8.15", "ENGLISH", "196", "29", "130");
        absenceBeanList.add(absenceBean1);
        AbsenceBean absenceBean2 = new AbsenceBean("2018 A/L SAT 6.15.8.15", "ENGLISH", "196", "29", "130");
        absenceBeanList.add(absenceBean1);
        AbsenceBean absenceBean3 = new AbsenceBean("2018 A/L SAT 6.15.8.15", "ENGLISH", "196", "29", "130");
        absenceBeanList.add(absenceBean1);
        AbsenceBean absenceBean4 = new AbsenceBean("2018 A/L SAT 6.15.8.15", "ENGLISH", "196", "29", "130");
        absenceBeanList.add(absenceBean1);


        absenceAdapter.notifyDataSetChanged();
    }


    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int i, int i1, int i2) {
        String months[] = {"January","February","March","April","May","June","July","August","September","October","November","December"};


        String dateEnd = i + "-" + (i1+1) + "-" + i2;

        Log.d("DATENIFRAS",dateEnd);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-d");
        try {
            Date dateEObj = sdf.parse(dateEnd);

            Calendar now = Calendar.getInstance();
            Calendar dateEndObj = Calendar.getInstance();
            dateEndObj.setTime(dateEObj);
            if (dateEndObj.before(now)) {
                date.setText(String.format("%02d", i2)+"");
                month.setText(months[i1]);
            } else {
                new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Invalid Date")
                        .setContentText("Please choose a valid date ")
                        .show();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
