package com.nertlab.cas.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.nertlab.cas.R;
import com.nertlab.cas.beans.AbsenceBean;
import com.nertlab.cas.beans.PendingBean;
import com.nertlab.cas.other.AbsenceAdapter;
import com.nertlab.cas.other.PendingAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class PendingFragment extends Fragment{

    private RecyclerView recyclerView;
    private List<PendingBean> pendingBeanList;
    private PendingAdapter pendingAdapter;

    public PendingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_pending, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        pendingBeanList = new ArrayList<>();
        pendingAdapter = new PendingAdapter(getActivity(), pendingBeanList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(pendingAdapter);

        prepareList();

        return rootView;
    }

    private void prepareList() {
        PendingBean pendingBean1 = new PendingBean("2018 A/L", "ENGLISH", "73", "44", "29");
        pendingBeanList.add(pendingBean1);
        PendingBean pendingBean2 = new PendingBean("GRADE 10 FRI 3.00-5.00", "ENGLISH", "73", "24", "42");
        pendingBeanList.add(pendingBean2);
        PendingBean pendingBean3 = new PendingBean("2018 A/L", "ENGLISH", "73", "44", "29");
        pendingBeanList.add(pendingBean3);
        PendingBean pendingBean4 = new PendingBean("2018 A/L", "ENGLISH", "73", "44", "29");
        pendingBeanList.add(pendingBean4);

        pendingAdapter.notifyDataSetChanged();
    }


}
