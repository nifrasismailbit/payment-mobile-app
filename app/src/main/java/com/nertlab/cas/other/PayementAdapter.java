package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.PaymentList;

import java.util.List;
public class PayementAdapter extends RecyclerView.Adapter<PayementAdapter.MyViewHolder> {

    private Context mContext;
    private List<Payment> paymentList;

    public PayementAdapter(Context context, List<Payment> paymentList) {
        this.mContext = context;
        this.paymentList = paymentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Payment payment = paymentList.get(position);

        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);

        holder.grade.setText(payment.getFinal_grade_id());
        holder.month.setText("(" + payment.getMonth() + ")");
        holder.cards.setText(payment.getNumber_of_cards() +" Cards X ");
        holder.rate.setText(payment.getFees() + "");
        holder.totalPrice.setText(payment.getSubtotal()+"");

       holder.cashImage.setColorFilter(Color.parseColor(payment.getColor()));

        GradientDrawable greenRoundShape = (GradientDrawable)holder.mLayout.getBackground();
        greenRoundShape.setColor(Color.parseColor(payment.getColor()));

        GradientDrawable totalRoundShape = (GradientDrawable)holder.totalPrice.getBackground();
        totalRoundShape.setColor(Color.parseColor(payment.getDarkColor()));

        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext,PaymentList.class);
                intent.putExtra("grade",payment.getFinal_grade_id());
                intent.putExtra("subject_id",payment.getSubject_id());
                intent.putExtra("month",payment.getMonth());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView grade,month,year,cards,rate,totalPrice;
        public ImageView cashImage;
        public RelativeLayout mLayout;
        public MyViewHolder(View itemView) {
            super(itemView);
            grade = (TextView)itemView.findViewById(R.id.grade);
            month = (TextView)itemView.findViewById(R.id.month);
            cards = (TextView)itemView.findViewById(R.id.cards);
            rate = (TextView)itemView.findViewById(R.id.rate);
            totalPrice = (TextView)itemView.findViewById(R.id.totalPrice);
            cashImage = (ImageView)itemView.findViewById(R.id.cashImage);
            mLayout = (RelativeLayout) itemView.findViewById(R.id.cardLayout);
        }
    }
}
