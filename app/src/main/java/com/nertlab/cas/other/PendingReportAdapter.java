package com.nertlab.cas.other;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.beans.AbsenceReportBean;
import com.nertlab.cas.beans.PendingReportBean;

import java.util.List;

public class PendingReportAdapter extends RecyclerView.Adapter<PendingReportAdapter.MyViewHolder> {

    private Context mContext;
    private List<PendingReportBean> pendingReportList;

    public PendingReportAdapter(Context context, List<PendingReportBean> pendingReportList) {
        this.mContext = context;
        this.pendingReportList = pendingReportList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_pending_report;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final PendingReportBean pendingReportBean = pendingReportList.get(position);
        holder.className.setText(pendingReportBean.getClass_name());
        holder.subject.setText(pendingReportBean.getSubject());
        holder.date.setText(pendingReportBean.getDateVal());

        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);


    }

    @Override
    public int getItemCount() {
        return pendingReportList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView className,subject,date;
        public MyViewHolder(View itemView) {
            super(itemView);
            className = itemView.findViewById(R.id.lblClass);
            subject = itemView.findViewById(R.id.lblSubject);
            date = itemView.findViewById(R.id.lblDate);
        }
    }
}
