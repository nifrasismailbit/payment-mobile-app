package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.MyClassList;
import com.nertlab.cas.activity.PaymentList;
import com.nertlab.cas.beans.MyClassBean;

import java.util.List;

public class MyClassAdapter extends RecyclerView.Adapter<MyClassAdapter.MyViewHolder> {

    private Context mContext;
    private List<MyClassBean> myClassBeanList;

    public MyClassAdapter(Context context, List<MyClassBean> myClassBeanList) {
        this.mContext = context;
        this.myClassBeanList = myClassBeanList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_my_classes;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final MyClassBean myClassBean = myClassBeanList.get(position);

        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);

        holder.lblClassName.setText(myClassBean.getClassName());
        holder.lblClassName.setText("SUBJECT : " + myClassBean.getSubject());
        holder.lblDay.setText("ON : " + myClassBean.getDay());
        holder.lblFees.setText("FEES : " + myClassBean.getFee());
        holder.lblRegistered.setText(myClassBean.getRegistered());

        holder.layoutCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext.getApplicationContext(), MyClassList.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return myClassBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView lblClassName,lblSubject,lblDay,lblFees,lblRegistered;
        public LinearLayout layoutCard;
        public MyViewHolder(View itemView) {
            super(itemView);
            lblClassName = (TextView)itemView.findViewById(R.id.lblClassName);
            lblSubject = (TextView)itemView.findViewById(R.id.lblSubject);
            lblDay = (TextView)itemView.findViewById(R.id.lblDay);
            lblFees = (TextView)itemView.findViewById(R.id.lblFees);
            lblRegistered = (TextView)itemView.findViewById(R.id.lblRegistered);
            layoutCard = (LinearLayout) itemView.findViewById(R.id.layoutCard);
        }
    }
}
