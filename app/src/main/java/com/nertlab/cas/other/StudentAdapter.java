package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.PaymentDetailActivity;

import java.util.List;

/**
 * Created by nifras on 7/28/17.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder> {

    private Context mContext;
    private List<Group> studentList;

    public StudentAdapter(Context context, List<Group> studentList) {
        this.mContext = context;
        this.studentList = studentList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_student;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Group student = studentList.get(position);
        Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");

        holder.studentName.setText(student.getStudent_name());
        holder.studentId.setText("Student ID:  " +  student.getStuden_id());
        holder.totalPrice.setText(student.getFees());
        //holder.totalPrice.setTypeface(font);

        holder.payementCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext, PaymentDetailActivity.class);
                intent.putExtra("invoice",student.getInvoice());
                intent.putExtra("date",student.getDate());
                intent.putExtra("time",student.getTime());
                intent.putExtra("class",student.getClass_name());
                intent.putExtra("paidForMonth",student.getPaid_for_month());
                intent.putExtra("gender",student.getGender());
                intent.putExtra("dateOfBirth",student.getDobyear() + "-" + student.getDobmonth() + "-" + student.getDobdate());
                intent.putExtra("studentName",student.getStudent_name());
                intent.putExtra("studentId",student.getStuden_id());
                intent.putExtra("totalPayment",student.getPaying());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView totalPrice,studentName,studentId;
        private RelativeLayout payementCard;
        public MyViewHolder(View itemView) {
            super(itemView);
            payementCard = (RelativeLayout)itemView.findViewById(R.id.payementCard);
            totalPrice = (TextView)itemView.findViewById(R.id.totalPrice);
            studentName = (TextView)itemView.findViewById(R.id.studentName);
            studentId = (TextView)itemView.findViewById(R.id.studentId);
        }



    }
}
