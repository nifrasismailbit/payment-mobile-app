package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.AbsenceReport;
import com.nertlab.cas.activity.AttendanceReport;
import com.nertlab.cas.beans.SingleStudentAttendance;

import java.util.List;

public class AbsenceListAdapter extends RecyclerView.Adapter<AbsenceListAdapter.MyViewHolder> {

    private Context mContext;
    private List<SingleStudentAttendance> singleStudentAttendanceList;

    public AbsenceListAdapter(Context context, List<SingleStudentAttendance> singleStudentAttendanceList) {
        this.mContext = context;
        this.singleStudentAttendanceList = singleStudentAttendanceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_student_attendance;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SingleStudentAttendance singleStudentAttendance = singleStudentAttendanceList.get(position);
        holder.studentName.setText(singleStudentAttendance.getStudentName());
        holder.timeValue.setText(singleStudentAttendance.getTime());

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext.getApplicationContext(), AbsenceReport.class));
            }
        });
        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);


    }

    @Override
    public int getItemCount() {
        return singleStudentAttendanceList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView studentName, timeValue;
        private RelativeLayout card;
        public MyViewHolder(View itemView) {
            super(itemView);
            studentName = itemView.findViewById(R.id.studentName);
            timeValue = itemView.findViewById(R.id.timeValue);
            card = itemView.findViewById(R.id.card_single_list);
        }
    }
}
