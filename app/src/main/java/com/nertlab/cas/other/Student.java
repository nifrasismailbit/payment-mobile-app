package com.nertlab.cas.other;

/**
 * Created by nifras on 7/28/17.
 */

public class Student {
    private String studentName,studentId,paymentMonth;
    private Double payment;
    private int grade,year;

    public Student(String studentName, String studentId, String paymentMonth, Double payment, int grade, int year) {
        this.studentName = studentName;
        this.studentId = studentId;
        this.paymentMonth = paymentMonth;
        this.payment = payment;
        this.grade = grade;
        this.year = year;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getPaymentMonth() {
        return paymentMonth;
    }

    public void setPaymentMonth(String paymentMonth) {
        this.paymentMonth = paymentMonth;
    }

    public Double getPayment() {
        return payment;
    }

    public void setPayment(Double payment) {
        this.payment = payment;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
