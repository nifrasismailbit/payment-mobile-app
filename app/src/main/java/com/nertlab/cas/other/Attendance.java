package com.nertlab.cas.other;

public class Attendance {
    private String class_name,subject,count;

    public Attendance(String class_name, String subject, String count) {
        this.class_name = class_name;
        this.subject = subject;
        this.count = count;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
