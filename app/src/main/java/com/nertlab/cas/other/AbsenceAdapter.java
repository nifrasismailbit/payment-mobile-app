package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.AbsenceList;
import com.nertlab.cas.activity.PaymentList;
import com.nertlab.cas.beans.AbsenceBean;

import java.util.List;

public class AbsenceAdapter extends RecyclerView.Adapter<AbsenceAdapter.MyViewHolder> {

    private Context mContext;
    private List<AbsenceBean> absenceBeanList;

    public AbsenceAdapter(Context context, List<AbsenceBean> absenceBeanList) {
        this.mContext = context;
        this.absenceBeanList = absenceBeanList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_absence;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final AbsenceBean absenceBean = absenceBeanList.get(position);

        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);

        holder.lblClassName.setText(absenceBean.getClassName());
        holder.lblSubject.setText(absenceBean.getSubject());
        holder.lblRegistered.setText(absenceBean.getRegistered());
        holder.lblAbsence.setText(absenceBean.getAbsence());
        holder.lblAttendance.setText(absenceBean.getAttendance());


        holder.layoutCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext.getApplicationContext(), AbsenceList.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return absenceBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView lblClassName,lblSubject,lblRegistered,lblAbsence,lblAttendance;
        public LinearLayout layoutCard;
        public MyViewHolder(View itemView) {
            super(itemView);
            lblClassName = (TextView)itemView.findViewById(R.id.lblClassName);
            lblSubject = (TextView)itemView.findViewById(R.id.lblSubject);
            lblRegistered = (TextView)itemView.findViewById(R.id.lblRegistered);
            lblAbsence = (TextView)itemView.findViewById(R.id.lblAbsence);
            lblAttendance = (TextView)itemView.findViewById(R.id.lblAttendance);
            layoutCard = (LinearLayout) itemView.findViewById(R.id.layoutCard);
        }
    }
}
