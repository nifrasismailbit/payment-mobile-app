package com.nertlab.cas.other;

/**
 * Created by nifras on 8/1/17.
 */

public class Group {
    private  String student_name, studen_id, fees, paying, invoice, date, time, paid_for_month,class_name, subject, gender,dobyear,dobmonth,dobdate;

    public Group(String student_name, String studen_id, String fees, String paying, String invoice, String date, String time, String paid_for_month, String class_name, String subject, String gender, String dobyear, String dobmonth, String dobdate) {
        this.student_name = student_name;
        this.studen_id = studen_id;
        this.fees = fees;
        this.paying = paying;
        this.invoice = invoice;
        this.date = date;
        this.time = time;
        this.paid_for_month = paid_for_month;
        this.class_name = class_name;
        this.subject = subject;
        this.gender = gender;
        this.dobyear = dobyear;
        this.dobmonth = dobmonth;
        this.dobdate = dobdate;
    }

    public String getStudent_name() {
        return student_name;
    }

    public void setStudent_name(String student_name) {
        this.student_name = student_name;
    }

    public String getStuden_id() {
        return studen_id;
    }

    public void setStuden_id(String studen_id) {
        this.studen_id = studen_id;
    }

    public String getFees() {
        return fees;
    }

    public void setFees(String fees) {
        this.fees = fees;
    }

    public String getPaying() {
        return paying;
    }

    public void setPaying(String paying) {
        this.paying = paying;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPaid_for_month() {
        return paid_for_month;
    }

    public void setPaid_for_month(String paid_for_month) {
        this.paid_for_month = paid_for_month;
    }

    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDobyear() {
        return dobyear;
    }

    public void setDobyear(String dobyear) {
        this.dobyear = dobyear;
    }

    public String getDobmonth() {
        return dobmonth;
    }

    public void setDobmonth(String dobmonth) {
        this.dobmonth = dobmonth;
    }

    public String getDobdate() {
        return dobdate;
    }

    public void setDobdate(String dobdate) {
        this.dobdate = dobdate;
    }
}
