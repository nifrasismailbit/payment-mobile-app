package com.nertlab.cas.other;

public class Payment {
    private String final_grade_id;
    private int number_of_cards;
    private String month;
    private String color;
    private String darkColor;
    private int fees;
    private int subtotal;
    private String subject_id;

    public Payment(String final_grade_id, int number_of_cards, String month, String color, String darkColor, int fees, int subtotal, String subject_id) {
        this.final_grade_id = final_grade_id;
        this.number_of_cards = number_of_cards;
        this.month = month;
        this.color = color;
        this.darkColor = darkColor;
        this.fees = fees;
        this.subtotal = subtotal;
        this.subject_id = subject_id;
    }

    public String getFinal_grade_id() {
        return final_grade_id;
    }

    public void setFinal_grade_id(String final_grade_id) {
        this.final_grade_id = final_grade_id;
    }

    public int getNumber_of_cards() {
        return number_of_cards;
    }

    public void setNumber_of_cards(int number_of_cards) {
        this.number_of_cards = number_of_cards;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDarkColor() {
        return darkColor;
    }

    public void setDarkColor(String darkColor) {
        this.darkColor = darkColor;
    }

    public int getFees() {
        return fees;
    }

    public void setFees(int fees) {
        this.fees = fees;
    }

    public int getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(int subtotal) {
        this.subtotal = subtotal;
    }

    public String getSubject_id() {
        return subject_id;
    }

    public void setSubject_id(String subject_id) {
        this.subject_id = subject_id;
    }
}
