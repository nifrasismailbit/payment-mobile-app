package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.AttendanceList;

import java.util.List;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {

    private Context mContext;
    private List<Attendance> attendanceList;

    public AttendanceAdapter(Context context, List<Attendance> attendanceList) {
        this.mContext = context;
        this.attendanceList = attendanceList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_attendance;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Attendance attendance = attendanceList.get(position);
        holder.className.setText(attendance.getClass_name());
        holder.subject.setText(attendance.getSubject());
        holder.attendanceCount.setText(attendance.getCount());

        holder.cardAttendance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(mContext.getApplicationContext(), AttendanceList.class);
                mContext.startActivity(intent);
            }
        });
        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);


    }

    @Override
    public int getItemCount() {
        return attendanceList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView className,subject,attendanceCount;
        private RelativeLayout cardAttendance;
        public MyViewHolder(View itemView) {
            super(itemView);
            className = itemView.findViewById(R.id.className);
            subject = itemView.findViewById(R.id.subject);
            attendanceCount = itemView.findViewById(R.id.attendanceCount);
            cardAttendance = itemView.findViewById(R.id.cardAttendance);
        }
    }
}
