package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.AttendanceList;
import com.nertlab.cas.activity.AttendanceReport;
import com.nertlab.cas.beans.AttendanceReportBean;

import java.util.List;

public class AttendanceReportAdapter extends RecyclerView.Adapter<AttendanceReportAdapter.MyViewHolder> {

    private Context mContext;
    private List<AttendanceReportBean> attendanceReportList;

    public AttendanceReportAdapter(Context context, List<AttendanceReportBean> attendanceReportList) {
        this.mContext = context;
        this.attendanceReportList = attendanceReportList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_attendance_report;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final AttendanceReportBean attendanceReportBean = attendanceReportList.get(position);
        holder.className.setText(attendanceReportBean.getClass_name());
        holder.subject.setText(attendanceReportBean.getSubject());
        holder.date.setText(attendanceReportBean.getDateVal());
        holder.time.setText(attendanceReportBean.gettime());

        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);


    }

    @Override
    public int getItemCount() {
        return attendanceReportList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView className,subject,date,time;
        public MyViewHolder(View itemView) {
            super(itemView);
            className = itemView.findViewById(R.id.lblClass);
            subject = itemView.findViewById(R.id.lblSubject);
            date = itemView.findViewById(R.id.lblDate);
            time = itemView.findViewById(R.id.lblTime);
        }
    }
}
