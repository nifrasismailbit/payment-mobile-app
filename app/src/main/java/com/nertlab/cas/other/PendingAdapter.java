package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.AbsenceList;
import com.nertlab.cas.activity.PendingList;
import com.nertlab.cas.beans.AbsenceBean;
import com.nertlab.cas.beans.PendingBean;

import java.util.List;

public class PendingAdapter extends RecyclerView.Adapter<PendingAdapter.MyViewHolder> {

    private Context mContext;
    private List<PendingBean> pendingBeanList;

    public PendingAdapter(Context context, List<PendingBean> pendingBeanList) {
        this.mContext = context;
        this.pendingBeanList = pendingBeanList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_pending;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final PendingBean pendingBean = pendingBeanList.get(position);

        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);

        holder.lblClassName.setText(pendingBean.getClassName());
        holder.lblSubject.setText(pendingBean.getSubject());
        holder.lblRegistered.setText(pendingBean.getRegistered());
        holder.lblPaid.setText(pendingBean.getPaid());
        holder.lblNotPaid.setText(pendingBean.getNotPaid());


        holder.layoutCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.startActivity(new Intent(mContext.getApplicationContext(), PendingList.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        return pendingBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView lblClassName,lblSubject,lblRegistered,lblPaid,lblNotPaid;
        public LinearLayout layoutCard;
        public MyViewHolder(View itemView) {
            super(itemView);
            lblClassName = (TextView)itemView.findViewById(R.id.lblClassName);
            lblSubject = (TextView)itemView.findViewById(R.id.lblSubject);
            lblRegistered = (TextView)itemView.findViewById(R.id.lblRegistered);
            lblPaid = (TextView)itemView.findViewById(R.id.lblPaid);
            lblNotPaid = (TextView)itemView.findViewById(R.id.lblNotPaid);
            layoutCard = (LinearLayout) itemView.findViewById(R.id.layoutCard);
        }
    }
}
