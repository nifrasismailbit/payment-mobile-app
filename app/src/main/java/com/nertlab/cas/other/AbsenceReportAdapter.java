package com.nertlab.cas.other;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.beans.AbsenceReportBean;

import java.util.List;

public class AbsenceReportAdapter extends RecyclerView.Adapter<AbsenceReportAdapter.MyViewHolder> {

    private Context mContext;
    private List<AbsenceReportBean> absenceReportList;

    public AbsenceReportAdapter(Context context, List<AbsenceReportBean> absenceReportList) {
        this.mContext = context;
        this.absenceReportList = absenceReportList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_absence_report;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final AbsenceReportBean absenceReportBean = absenceReportList.get(position);
        holder.className.setText(absenceReportBean.getClass_name());
        holder.subject.setText(absenceReportBean.getSubject());
        holder.date.setText(absenceReportBean.getDateVal());

        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);


    }

    @Override
    public int getItemCount() {
        return absenceReportList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private TextView className,subject,date;
        public MyViewHolder(View itemView) {
            super(itemView);
            className = itemView.findViewById(R.id.lblClass);
            subject = itemView.findViewById(R.id.lblSubject);
            date = itemView.findViewById(R.id.lblDate);
        }
    }
}
