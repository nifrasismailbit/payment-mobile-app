package com.nertlab.cas.other;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.activity.AttendanceReport;
import com.nertlab.cas.beans.SingleMyClassStudentBean;
import com.nertlab.cas.beans.SingleStudentAttendance;

import java.util.List;

public class MyClassListAdapter extends RecyclerView.Adapter<MyClassListAdapter.MyViewHolder> {

    private Context mContext;
    private List<SingleMyClassStudentBean> singleMyClassStudentBeanList;

    public MyClassListAdapter(Context context, List<SingleMyClassStudentBean> singleMyClassStudentBeanList) {
        this.mContext = context;
        this.singleMyClassStudentBeanList = singleMyClassStudentBeanList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int layoutIdForListItem = R.layout.card_student_my_class;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachedToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem,parent,shouldAttachedToParentImmediately);
        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SingleMyClassStudentBean singleMyClassStudentBean = singleMyClassStudentBeanList.get(position);
        holder.studentName.setText(singleMyClassStudentBean.getStudentName());


        //Typeface font=Typeface.createFromAsset(mContext.getAssets(), "fonts/abc.ttf");
        //holder.grade.setTypeface(font);
        //holder.month.setTypeface(font);
        //holder.year.setTypeface(font);
       // holder.rate.setTypeface(font);
       // holder.totalPrice.setTypeface(font);


    }

    @Override
    public int getItemCount() {
        return singleMyClassStudentBeanList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        private TextView studentName;
        public MyViewHolder(View itemView) {
            super(itemView);
            studentName = itemView.findViewById(R.id.studentName);
        }
    }
}
