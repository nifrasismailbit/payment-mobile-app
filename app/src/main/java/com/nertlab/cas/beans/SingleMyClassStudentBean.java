package com.nertlab.cas.beans;

/**
 * Created by nifras on 9/13/17.
 */

public class SingleMyClassStudentBean {
    private String studentName;

    public SingleMyClassStudentBean(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}
