package com.nertlab.cas.beans;

/**
 * Created by safrana on 9/14/17.
 */

public class PendingBean {
    private String className, subject, registered, paid, notPaid;

    public PendingBean(String className, String subject, String registered, String paid, String notPaid) {
        this.className = className;
        this.subject = subject;
        this.registered = registered;
        this.paid = paid;
        this.notPaid = notPaid;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getNotPaid() {
        return notPaid;
    }

    public void setNotPaid(String notPaid) {
        this.notPaid = notPaid;
    }
}
