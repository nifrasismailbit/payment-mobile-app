package com.nertlab.cas.beans;

/**
 * Created by nifras on 9/13/17.
 */

public class AbsenceReportBean {
    private String class_name,subject,dateVal;

    public AbsenceReportBean(String class_name, String subject,  String dateVal) {
        this.class_name = class_name;
        this.subject = subject;
        this.dateVal = dateVal;
    }



    public String getClass_name() {
        return class_name;
    }

    public void setClass_name(String class_name) {
        this.class_name = class_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDateVal() {
        return dateVal;
    }

    public void setDateVal(String dateVal) {
        this.dateVal = dateVal;
    }
}
