package com.nertlab.cas.beans;

/**
 * Created by nifras on 8/25/17.
 */

public class SingleStudentAttendance {
    private String studentName,time;

    public SingleStudentAttendance(String studentName, String time) {
        this.studentName = studentName;
        this.time = time;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
