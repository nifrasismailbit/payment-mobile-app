package com.nertlab.cas.beans;

import com.nertlab.cas.other.Payment;

/**
 * Created by nifras on 7/31/17.
 */

public class PayementBean {
    private String total_attendance,total_students,total_payments;
    private Payment[] overview_payments;

    public PayementBean(String total_attendance, String total_students, String total_payments, Payment[] overview_payments) {
        this.total_attendance = total_attendance;
        this.total_students = total_students;
        this.total_payments = total_payments;
        this.overview_payments = overview_payments;
    }

    public String getTotal_attendance() {
        return total_attendance;
    }

    public void setTotal_attendance(String total_attendance) {
        this.total_attendance = total_attendance;
    }

    public String getTotal_students() {
        return total_students;
    }

    public void setTotal_students(String total_students) {
        this.total_students = total_students;
    }

    public String getTotal_payments() {
        return total_payments;
    }

    public void setTotal_payments(String total_payments) {
        this.total_payments = total_payments;
    }

    public Payment[] getOverview_payments() {
        return overview_payments;
    }

    public void setOverview_payments(Payment[] overview_payments) {
        this.overview_payments = overview_payments;
    }
}
