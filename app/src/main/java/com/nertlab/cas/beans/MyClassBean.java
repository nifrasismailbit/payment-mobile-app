package com.nertlab.cas.beans;

/**
 * Created by nifras on 9/13/17.
 */

public class MyClassBean {
    private String className,subject,day,fee,registered;

    public MyClassBean(String className, String subject, String day, String fee, String registered) {
        this.className = className;
        this.subject = subject;
        this.day = day;
        this.fee = fee;
        this.registered = registered;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }
}
