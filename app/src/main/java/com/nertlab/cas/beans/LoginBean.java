package com.nertlab.cas.beans;

/**
 * Created by nifras on 7/30/17.
 */

public class LoginBean {
        private String message,token,name,phone_number,class_id;
        private Boolean status;
    public LoginBean(Boolean status, String message, String token, String name, String phone_number,String class_id) {
        this.status = status;
        this.message = message;
        this.token = token;
        this.name = name;
        this.phone_number = phone_number;
        this.class_id = class_id;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getClass_id() {
        return class_id;
    }

    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
