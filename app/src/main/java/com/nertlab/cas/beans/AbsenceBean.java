package com.nertlab.cas.beans;

/**
 * Created by safrana on 9/13/17.
 */

public class AbsenceBean {
    private String className, subject, registered, attendance, absence;

    public AbsenceBean(String className, String subject, String registered, String attendance, String absence) {
        this.className = className;
        this.subject = subject;
        this.registered = registered;
        this.attendance = attendance;
        this.absence = absence;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getAbsence() {
        return absence;
    }

    public void setAbsence(String absence) {
        this.absence = absence;
    }
}
