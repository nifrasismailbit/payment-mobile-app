package com.nertlab.cas.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.nertlab.cas.R;
import com.nertlab.cas.beans.AttendanceReportBean;
import com.nertlab.cas.beans.SingleStudentAttendance;
import com.nertlab.cas.other.AttendanceReportAdapter;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AttendanceReport extends AppCompatActivity {

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    private RecyclerView recycler_view_attendance_report_list;
    private AttendanceReportAdapter attendanceReportAdapter;
    private List<AttendanceReportBean> attendanceReportList;

    private SweetAlertDialog pDialog;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_report);
        intent = getIntent();

        sharedpreferences = getSharedPreferences("my-pref", Context.MODE_PRIVATE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Attendance Report");

        recycler_view_attendance_report_list = (RecyclerView) findViewById(R.id.recycler_view_attendance_report_list);

        attendanceReportList = new ArrayList<>();
        attendanceReportAdapter = new AttendanceReportAdapter(this, attendanceReportList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recycler_view_attendance_report_list.setLayoutManager(mLayoutManager);
        recycler_view_attendance_report_list.setAdapter(attendanceReportAdapter);
        prepareList();
    }

    private void prepareList() {
        AttendanceReportBean attendanceReportBean1 = new AttendanceReportBean("GRADE 6 SATURDAY 4.45-6.30","English","April 8, 2017","4:49 PM");
        attendanceReportList.add(attendanceReportBean1);

        AttendanceReportBean attendanceReportBean2 = new AttendanceReportBean("GRADE 6 SATURDAY 4.45-6.30","English","April 8, 2017","4:49 PM");
        attendanceReportList.add(attendanceReportBean1);

        AttendanceReportBean attendanceReportBean3 = new AttendanceReportBean("GRADE 6 SATURDAY 4.45-6.30","English","April 8, 2017","4:49 PM");
        attendanceReportList.add(attendanceReportBean1);

        attendanceReportAdapter.notifyDataSetChanged();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}

