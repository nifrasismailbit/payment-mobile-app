package com.nertlab.cas.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.nertlab.cas.R;

public class PrivacyPolicyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
    }
}
