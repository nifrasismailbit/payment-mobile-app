package com.nertlab.cas.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nertlab.cas.R;
import com.nertlab.cas.beans.MyClassBean;
import com.nertlab.cas.beans.SingleMyClassStudentBean;
import com.nertlab.cas.other.MyClassListAdapter;

import java.util.ArrayList;
import java.util.List;

public class MyClassList extends AppCompatActivity {

    private MyClassListAdapter myClassListAdapter;
    private List<SingleMyClassStudentBean> singleMyClassStudentBeanList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_class_list);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Registered Students");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        singleMyClassStudentBeanList = new ArrayList<>();
        myClassListAdapter = new MyClassListAdapter(this, singleMyClassStudentBeanList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(myClassListAdapter);
        prepareList();

    }

    private void prepareList() {
        SingleMyClassStudentBean singleMyClassStudentBean1 = new SingleMyClassStudentBean("W A WIJAYA SRI");
        singleMyClassStudentBeanList.add(singleMyClassStudentBean1);
SingleMyClassStudentBean singleMyClassStudentBean2 = new SingleMyClassStudentBean("W A WIJAYA SRI");
        singleMyClassStudentBeanList.add(singleMyClassStudentBean1);
SingleMyClassStudentBean singleMyClassStudentBean3 = new SingleMyClassStudentBean("W A WIJAYA SRI");
        singleMyClassStudentBeanList.add(singleMyClassStudentBean1);
SingleMyClassStudentBean singleMyClassStudentBean4 = new SingleMyClassStudentBean("W A WIJAYA SRI");
        singleMyClassStudentBeanList.add(singleMyClassStudentBean1);
SingleMyClassStudentBean singleMyClassStudentBean5 = new SingleMyClassStudentBean("W A WIJAYA SRI");
        singleMyClassStudentBeanList.add(singleMyClassStudentBean1);
SingleMyClassStudentBean singleMyClassStudentBean6 = new SingleMyClassStudentBean("W A WIJAYA SRI");
        singleMyClassStudentBeanList.add(singleMyClassStudentBean1);
SingleMyClassStudentBean singleMyClassStudentBean7 = new SingleMyClassStudentBean("W A WIJAYA SRI");
        singleMyClassStudentBeanList.add(singleMyClassStudentBean1);
SingleMyClassStudentBean singleMyClassStudentBean8 = new SingleMyClassStudentBean("W A WIJAYA SRI");
        singleMyClassStudentBeanList.add(singleMyClassStudentBean1);



        myClassListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
