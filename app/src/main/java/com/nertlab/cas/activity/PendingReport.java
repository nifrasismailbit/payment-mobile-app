package com.nertlab.cas.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.nertlab.cas.R;
import com.nertlab.cas.beans.AbsenceReportBean;
import com.nertlab.cas.beans.PendingReportBean;
import com.nertlab.cas.other.AbsenceReportAdapter;
import com.nertlab.cas.other.PendingReportAdapter;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class PendingReport extends AppCompatActivity {

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    private RecyclerView recycler_view_pending_report_list;
    private PendingReportAdapter pendingReportAdapter;
    private List<PendingReportBean> pendingReportList;

    private SweetAlertDialog pDialog;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_report);
        intent = getIntent();

        sharedpreferences = getSharedPreferences("my-pref", Context.MODE_PRIVATE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pending Payments");

        recycler_view_pending_report_list = (RecyclerView) findViewById(R.id.recycler_view_pending_report_list);

        pendingReportList = new ArrayList<>();
        pendingReportAdapter = new PendingReportAdapter(this, pendingReportList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recycler_view_pending_report_list.setLayoutManager(mLayoutManager);
        recycler_view_pending_report_list.setAdapter(pendingReportAdapter);
        prepareList();
    }

    private void prepareList() {
        PendingReportBean pendingReportBean1 = new PendingReportBean("GRADE 10 FRI 3.00-5.00","SINHALA","April 2017");
        pendingReportList.add(pendingReportBean1);

        PendingReportBean pendingReportBean2 = new PendingReportBean("GRADE 10 FRI 3.00-5.00","English","April 2017");
            pendingReportList.add(pendingReportBean2);

        PendingReportBean pendingReportBean3 = new PendingReportBean("GRADE 10 FRI 3.00-5.00","English","April 2017");
            pendingReportList.add(pendingReportBean3);



        pendingReportAdapter.notifyDataSetChanged();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}

