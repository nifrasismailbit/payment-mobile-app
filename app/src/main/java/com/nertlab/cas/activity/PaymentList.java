package com.nertlab.cas.activity;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nertlab.cas.ApplicationContstant;
import com.nertlab.cas.R;
import com.nertlab.cas.other.Group;
import com.nertlab.cas.other.Payment;
import com.nertlab.cas.other.Student;
import com.nertlab.cas.other.StudentAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class PaymentList extends AppCompatActivity {

    private RecyclerView recycler_view_payment_list;
    private StudentAdapter studentAdapter;
    private List<Group> studentList;

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    private SweetAlertDialog pDialog;

    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_list);

        intent = getIntent();

        sharedpreferences = getSharedPreferences("my-pref", Context.MODE_PRIVATE);

        TextView header = (TextView)findViewById(R.id.header);
        String startDate = sharedpreferences.getString("start_date_view","");
        String endDate = sharedpreferences.getString("end_date_view","");
        String grade = intent.getExtras().get("grade").toString();
        header.setText(grade);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(startDate + " to " + endDate);

        recycler_view_payment_list = (RecyclerView) findViewById(R.id.recycler_view_payment_list);

        studentList = new ArrayList<>();
        studentAdapter = new StudentAdapter(this, studentList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recycler_view_payment_list.setLayoutManager(mLayoutManager);
        recycler_view_payment_list.setAdapter(studentAdapter);
        prepareList();
    }

    private void prepareList() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
        String class_id = sharedpreferences.getString("class_id","");
        String token = sharedpreferences.getString("token","");
        String startDate = sharedpreferences.getString("start_date","");
        String endDate = sharedpreferences.getString("end_date","");
        String month = intent.getExtras().get("month").toString();
        String subjectId = intent.getExtras().get("subject_id").toString();


        OkHttpClient client = new OkHttpClient();
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cfms_payments_from",startDate );
            jsonObject.put("cfms_payments_to", endDate);
            jsonObject.put("class_id", class_id);
            jsonObject.put("cfms_token", token);
            jsonObject.put("subject_id", subjectId);
            jsonObject.put("month", month);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        okhttp3.RequestBody body = RequestBody.create(JSON, jsonObject.toString());

        okhttp3.Request request = new Request.Builder()
                .url(ApplicationContstant.group)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.cancel();
                        Toast.makeText(PaymentList.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseString = response.body().string();
                    final JSONObject object = new JSONObject(responseString);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });
                    JSONArray jsonArray = object.getJSONArray("reciept_payments");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Gson gson = new Gson();
                        final Group group = gson.fromJson(jsonObject.toString(), Group.class);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateUI(group);
                            }
                        });

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void updateUI(Group group) {
        if (group != null) {
            studentList.add(group);
        }
        studentAdapter.notifyDataSetChanged();
        pDialog.cancel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.main, menu);
            // Retrieve the SearchView and plug it into SearchManager
            final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
            SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            return true;
        }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
