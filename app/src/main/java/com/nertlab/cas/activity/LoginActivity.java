package com.nertlab.cas.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nertlab.cas.ApplicationContstant;
import com.nertlab.cas.R;
import com.nertlab.cas.beans.LoginBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginActivity extends BaseActivity implements View.OnClickListener,AdapterView.OnItemSelectedListener {

    private EditText username,password;
    private Spinner spinner;
    private Button btnSigin;
    private SweetAlertDialog pDialog;

    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sharedpreferences = getSharedPreferences("my-pref", Context.MODE_PRIVATE);
        final String token = sharedpreferences.getString("token", "");
        if(!isInterentAvailable()){
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("No, Internet Connection")
                    .setContentText("Please check your internet connectivity")
                    .setConfirmText("TryAgain")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            if(isInterentAvailable()) {
                                sDialog.dismissWithAnimation();
                                loadSpinnerData();
                                if (!token.isEmpty()) {
                                    Intent i = new Intent();
                                    i.setClass(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                }
                            }
                        }
                    })
                    .show();
        }



        if(isInterentAvailable()) {
            if (!token.isEmpty()) {
                Intent i = new Intent();
                i.setClass(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
            loadSpinnerData();
        }
        editor = sharedpreferences.edit();

        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);


        spinner = (Spinner)findViewById(R.id.spinner);

        spinner.setOnItemSelectedListener(this);

        btnSigin = (Button)findViewById(R.id.btnSignin);
        btnSigin.setOnClickListener(this);


    }

    private void loadSpinnerData() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
        OkHttpClient client = new OkHttpClient();
        okhttp3.Request request = new Request.Builder()
                .get()
                .url(ApplicationContstant.classes)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.cancel();
                        Toast.makeText(LoginActivity.this, "Invalid Request", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseString = response.body().string();

                    final JSONArray jsonArray = new JSONArray(responseString);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                updateUI(jsonArray);
                            }
                        });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    List<String> listIds = new ArrayList<String>();
    private void updateUI(JSONArray jsonArray){
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                list.add(jsonObject.get("class_name").toString());
                listIds.add(jsonObject.get("id").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        pDialog.cancel();
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSignin:
                doLogin();
                break;
        }
    }

    private void doLogin() {
        pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText("Loading");
        pDialog.setCancelable(false);
        pDialog.show();
        String username = this.username.getText().toString();
        String password = this.password.getText().toString();

        OkHttpClient client = new OkHttpClient();

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("cfms_username",username);
            jsonObject.put("cfms_password",password);
            jsonObject.put("class_id",selectedClassId);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        okhttp3.RequestBody body = RequestBody.create(JSON, jsonObject.toString());


        okhttp3.Request request = new Request.Builder()
                .url(ApplicationContstant.login)
                .post(body)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        pDialog.cancel();
                        Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try {
                    String responseString = response.body().string();

                    JSONObject jsonObject = new JSONObject(responseString);
                    Gson gson = new Gson();
                    final LoginBean loginBean = gson.fromJson(jsonObject.toString(), LoginBean.class);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            updateUI(loginBean);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void updateUI(LoginBean loginBean) {
        pDialog.cancel();
        if(loginBean.getStatus()){
            editor.putString("token", loginBean.getToken());
            editor.putString("name", loginBean.getName());
            editor.putString("phone_number", loginBean.getPhone_number());
            editor.putString("class_id", loginBean.getClass_id());
            editor.commit();

            Intent mainActivity = new Intent();
            mainActivity.setClass(getApplicationContext(),MainActivity.class);
            startActivity(mainActivity);
        }else{
            new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText("Invalid Login")
                    .setContentText("Please check your username and password")
                    .setConfirmText("Try Again")
                    .show();
        }

    }

    private String selectedClassId = "";
    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        selectedClassId = listIds.get(i);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
