package com.nertlab.cas.activity;

import android.app.SearchManager;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.nertlab.cas.R;
import com.nertlab.cas.beans.SingleStudentAttendance;
import com.nertlab.cas.other.AbsenceListAdapter;

import java.util.ArrayList;
import java.util.List;

public class AbsenceList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private AbsenceListAdapter absenceListAdapter;
    private List<SingleStudentAttendance> singleStudentAttendanceList;
    //private SharedPreferences sharedpreferences;
    //private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absence_list);

        //sharedpreferences = getSharedPreferences("my-pref", Context.MODE_PRIVATE);

        //String startDate = sharedpreferences.getString("attendance_start_date_view","");
        //String endDate = sharedpreferences.getString("attendance_end_date_view","");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("05 Aug - Absence");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        singleStudentAttendanceList = new ArrayList<>();
        absenceListAdapter = new AbsenceListAdapter(this, singleStudentAttendanceList);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 1);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(absenceListAdapter);
        prepareList();

    }

    private void prepareList() {
        SingleStudentAttendance singleStudentAttendance1 = new SingleStudentAttendance("W A WIJAYA SRI","4:15 PM");
        singleStudentAttendanceList.add(singleStudentAttendance1);

        SingleStudentAttendance singleStudentAttendance2 = new SingleStudentAttendance("W A SAMIRA RATHNA","5:35 PM");
        singleStudentAttendanceList.add(singleStudentAttendance2);

        SingleStudentAttendance singleStudentAttendance3 = new SingleStudentAttendance("W A GUNATHILAKA","1:35 AM");
        singleStudentAttendanceList.add(singleStudentAttendance3);

        absenceListAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
