package com.nertlab.cas.activity;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.ColorInt;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.nertlab.cas.R;

public class PaymentDetailActivity extends AppCompatActivity {

    private TextView lblInvoice, lblDate, lblTime, lblDateOfBirth, lblClass,totalValue,lblPaidFor,lblOtherDetails,lblGender;
    private TextView txtInvoice, txtDate, txtTime, txtDateOfBirth, txtClass,txtTotalValue,txtPaidFor,txtGender,txtStudentName,txtStudentId;

    ImageView redRound, greenRound;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Payment Receipt");

        Typeface font=Typeface.createFromAsset(getAssets(), "fonts/abc.ttf");

        Intent intent = getIntent();

        lblInvoice = (TextView)findViewById(R.id.lblInvoiceNo);
        lblGender = (TextView)findViewById(R.id.lblGender);
        lblDate = (TextView)findViewById(R.id.lblDate);
        lblTime = (TextView)findViewById(R.id.lblTime);
        lblPaidFor = (TextView)findViewById(R.id.lblPaidiFor);
        lblClass = (TextView)findViewById(R.id.lblClass);
        lblOtherDetails = (TextView)findViewById(R.id.lblOtherDetails);
        lblDateOfBirth = (TextView)findViewById(R.id.lblDateOfBirth);

        txtInvoice = (TextView)findViewById(R.id.invoiceNo);
        txtGender = (TextView)findViewById(R.id.gender);
        txtDate = (TextView)findViewById(R.id.dateValue);
        txtTime = (TextView)findViewById(R.id.timeValue);
        txtPaidFor = (TextView)findViewById(R.id.paidForValue);
        txtClass = (TextView)findViewById(R.id.classValue);
        txtDateOfBirth = (TextView)findViewById(R.id.dateOfBirth);
        txtTotalValue = (TextView)findViewById(R.id.totalPrice);
        txtStudentName = (TextView)findViewById(R.id.studentName);
        txtStudentId = (TextView)findViewById(R.id.studentId);

        txtInvoice.setText(intent.getExtras().getString("invoice"));
        txtGender.setText(intent.getExtras().getString("gender"));
        txtDate.setText(intent.getExtras().getString("date"));
        txtTime.setText(intent.getExtras().getString("time"));
        txtPaidFor.setText(intent.getExtras().getString("paidForMonth"));
        txtClass.setText(intent.getExtras().getString("class"));
        txtDateOfBirth.setText(intent.getExtras().getString("dateOfBirth"));
        txtStudentName.setText(intent.getExtras().getString("studentName"));
        txtStudentId.setText("Student Id : "+intent.getExtras().getString("studentId"));
        txtTotalValue.setText(intent.getExtras().getString("totalPayment"));

        redRound = (ImageView)findViewById(R.id.redRound);
        greenRound = (ImageView)findViewById(R.id.greenRound);

        GradientDrawable redRoundShape = (GradientDrawable)redRound.getBackground();
        redRoundShape.setColor(Color.parseColor("#62cca3"));

        GradientDrawable greenRoundShape = (GradientDrawable)greenRound.getBackground();
        greenRoundShape.setColor(Color.parseColor("#109964"));

        setColor(true);

    }

    private void setColor(boolean b) {
        String color = "#FF0000";
        if(b){
            color = "#62cca3";
        }

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor(color)));

        lblInvoice.setTextColor(Color.parseColor(color));
        lblDate.setTextColor(Color.parseColor(color));
        lblTime.setTextColor(Color.parseColor(color));
        lblClass.setTextColor(Color.parseColor(color));
        lblPaidFor.setTextColor(Color.parseColor(color));
        lblOtherDetails.setTextColor(Color.parseColor(color));
        lblGender.setTextColor(Color.parseColor(color));
        lblDateOfBirth.setTextColor(Color.parseColor(color));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
